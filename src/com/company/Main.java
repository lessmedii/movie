package com.company;

import Domain.Movie;
import Domain.Client;
import Repository.MovieRepository;
import Repository.ClientRepository;
import Service.ClientService;
import Service.MovieService;

import java.util.Scanner;

public class Main {
    private static Scanner scanner;
    public static void main(String[] args) {
	// write your code here
        Movie m1 = new Movie("1","test1", 10);
        Movie m2 = new Movie("1","test2", 10);
        Movie m3 = new Movie("1","test3", 10);

        Client c1 = new Client("Mihai","1234567834");
        Client c2 = new Client("Ionut","1256738904");
        Client c3 = new Client("Camelia","678901234");

        MovieRepository mr = new MovieRepository();
        ClientRepository cr = new ClientRepository();

        MovieService movieService=new MovieService ( mr );
        movieService.addOrUpdate ( m1.getId (), m1.getName (), m1.getPrice () );
        movieService.addOrUpdate ( m2.getId (), m2.getName (), m2.getPrice () );
        movieService.addOrUpdate ( m3.getId (), m3.getName (), m3.getPrice () );

        ClientService clientService=new ClientService ( cr );
        clientService.addOrUpdate ( c1.getName (), c1.getCNP () );
        clientService.addOrUpdate ( c2.getName (), c1.getCNP () );
        clientService.addOrUpdate ( c3.getName (), c1.getCNP () );
    }
}
