package domain.validators;

/**
 * Created by D-E V
 */
public interface Validator<T> {
    void validate(T entity) throws ValidatorException;
}
