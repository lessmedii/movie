package Domain;

public class Client {

    private String name;
    private String CNP;

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", CNP=" + CNP +
                '}';
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(int CNP) {
        this.CNP = CNP;
    }

    public Client(String name, String CNP) {
        this.name = name;
        this.CNP = CNP;
    }
}
