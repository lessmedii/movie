
package Service;
import Domain.Client;
import Repository.ClientRepository;
import java.util.List;

public class ClientService {

    private ClientRepository<Client> repository;

    public ClientService(ClientRepository cr) {
        this.repository = cr;
    }

    public void setRepository(ClientRepository<Client> repository) {
        this.repository = repository;
    }

    public ClientRepository<Client> getRepository() {
        return repository;
    }

    public void addOrUpdate(String name, String CNP) {
        Client existing = repository.findByCNP(CNP);
        if (existing != null) {
            // keep unchanged fields as they were
            if (CNP.isEmpty()) {
                CNP = existing.getCNP();
            }
            if (name.isEmpty()) {
                name = existing.getName();
            }
        }
        Client client = new Client(name, CNP);
        repository.addOrUpdateClient(client);
    }

    public void removeClients(String CNP) {
        repository.removeClient(CNP);
    }

    public List<Client> getAllClients() {
        return repository.getAllClients();
    }
}

