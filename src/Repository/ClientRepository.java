package Repository;

import Domain.Client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientRepository<C extends Client> { //aici C mosteneste clasa Client

    private Map<String, C> listClients = new HashMap<>();      //aici am initializat o lista de client

    public void addOrUpdateClient(C entity) {                //adaugare sau modificare Client
        listClients.put(entity.getCNP(), entity);
    }

    public C findByCNP(String CNP) {
        return listClients.get(CNP);
    }

    public void removeClient(String CNP) {                     //stergere client dupa id
        if (!listClients.containsKey(CNP)) {                            //aici am pus conditie in cazul in care nu gaseste clientul
            System.out.println("The Client is not in the list");         //cu cnp-ul x sa dea mesaj
        } else {
            listClients.remove(CNP);
        }
    }

    public List<C> getAllClients() {                                  //aici ne afiseaza lista de clienti
        return new ArrayList<C>(listClients.values());
    }
}
